var express = require('express');
var app = express();

const shoppingController = require('./src/controllers/shopping.controller');
const biggerShoppingController = require('./src/controllers/biggerShopping.controller');
const customerTrueController = require('./src/controllers/customerTrue.controller');
const recomendController = require('./src/controllers/recomend.controller');
const helpers = require('./src/helpers/general.helpers');

const url_clientes = "http://www.mocky.io/v2/598b16291100004705515ec5";
const url_compras = "http://www.mocky.io/v2/598b16861100004905515ec7";

app.get('/', function(req, res) {
    res.status(200).send({ message: "RESTful Iniciado." });
});

// Retornando Total de compras de clientes
app.get('/shopping', async function(req, res) {
    var lista_clientes = await helpers.getData(url_clientes);
    var lista_compras = await helpers.arrumaCPF(await helpers.getData(url_compras));
    var lista_valores = await shoppingController.somaValoresTotais(lista_compras);
    var order = await shoppingController.ordenaCompras(lista_valores, lista_clientes);
    res.send(order);
});

// Retornando Maior compra de cada ano (2014, 2015, 2016)
app.get('/bigger-shopping/:ano', async function(req, res) {
    var lista_clientes = await helpers.getData(url_clientes);
    var lista_compras = await helpers.arrumaCPF(await helpers.getData(url_compras));
    var maiorCompra = await biggerShoppingController.maiorCompraUnica(lista_compras, req.params.ano, lista_clientes);
    res.send(maiorCompra);
});

// Retornando a lista de clientes e o nível de fidelidade 
// Parametro opcional: tamanho do ranking de clientes fieis
app.get('/customer-true/:top?', async function(req, res) {
    var lista_clientes = await helpers.getData(url_clientes);
    var lista_compras = await helpers.arrumaCPF(await helpers.getData(url_compras));
    var lista_fieis = await customerTrueController.clientesFieis(lista_compras, lista_clientes, req.params.top);
    res.send(lista_fieis);
});

// Retornando recomendação por cliente
// Parametro opcional de entrada de tipo (variedade, país, categoria)
app.get('/recommendation/:cliente/:tipo?', async function(req, res) {
    var lista_clientes = await helpers.getData(url_clientes);
    var lista_compras = await helpers.arrumaCPF(await helpers.getData(url_compras));
    var sisRec = await recomendController.sistemaRecomendacao(lista_compras);
    var recomendado = await recomendController.recomendaVinho(sisRec, req.params.cliente, lista_clientes, lista_compras, req.params.tipo);
    recomendado === false ? res.status(404).send({message: "Nada a recomendar"}) : res.status(200).send({message: "Baseado no seu gosto em vinhos recomendamos: " + recomendado});
    
});

app.listen(3333, function() {
    console.log('App listening on port 3333!');
});